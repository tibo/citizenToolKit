<?php 
 /**
  * Display the directory of back office
  * @param String $id Not mandatory : if specify, look for the person with this Id. 
  * Else will get the id of the person logged
  * @return type
  */
class ReferenceAction extends CAction
{
    public function run( $tpl=null, $view=null )
    {
        $controller = $this->getController();

      //get The person Id
      if (empty($id)) {
          if ( empty( Yii::app()->session["userId"] ) ) {
              $controller->redirect(Yii::app()->homeUrl);
          } else {
              $id = Yii::app()->session["userId"];
          }
      }

      $limitMin=0;
      $stepLim=100;
      if(@$_POST["page"]){
        $limitMin=$limitMin+(100*$_POST["page"]);
      }
      $searchLocality = isset($_POST['locality']) ? $_POST['locality'] : null;
       //$localities = isset($post['localities']) ? $post['localities'] : null;
      $searchType = isset($post['searchType']) ? $post['searchType'] : null;
      $searchTags = isset($_POST['searchTag']) ? $_POST['searchTag'] : null;
      $country = isset($_POST['country']) ? $_POST['country'] : "";
      $sourceKey = !empty($_POST['sourceKey']) ? $_POST['sourceKey'] : null;
      if(isset(Yii::app()->session["custom"]) 
        && isset(Yii::app()->session["custom"]["slug"]))
        $sourceKey=Yii::app()->session["custom"]["slug"];

      $mode = !empty($_POST['mode']) ? $_POST['mode'] : null;
      $search="";
      if(@$_POST["text"] && !empty($_POST["text"])){
        $search = trim(urldecode($_POST['text']));
      }
      $query = array();
      $queryNews=array();
      $query = Search::searchString($search, $query);
      if(count($searchTags) > 1  || count($searchTags) == 1 && $searchTags[0] != "" ){
        if( (strcmp($filter, Classified::COLLECTION) != 0 && self::typeWanted(Classified::COLLECTION, $searchType)) ||
          (strcmp($filter, Place::COLLECTION) != 0 && self::typeWanted(Place::COLLECTION, $searchType)) ){
            $queryTags =  Search::searchTags($searchTags, '$all') ;
        }
        else 
          $queryTags =  Search::searchTags($searchTags) ;
        if(!empty($queryTags))
          $query = array('$and' => array( $query , $queryTags) );
      }
      if(!empty($searchLocality))
        $query = Search::searchLocality($searchLocality, $query);
      $query = array('$and' => array( $query , array( "disabled" => array('$exists' => false) ) ) );
      if(!isset($tpl)){
          $tmpSourceKey = array();
          if($sourceKey != null && $sourceKey != ""){
            //Several Sourcekey
            if(is_array($sourceKey)){
              foreach ($sourceKey as $value) {
                $tmpSourceKey[] = $value;
              }
            }//One Sourcekey
            else{
              $tmpSourceKey[] = $sourceKey;
            }
            $countRef=0;
            $countSource=0;
            $origin=(@Yii::app()->session["custom"]) ? "costum" : "network";
            foreach($_POST["initType"] as $data){
               $countRef+=PHDB::count( $data , array('$and'=>array( $query, array("reference.".$origin => array('$in' => $tmpSourceKey)))));
            }
            $params["countMenu"]["reference"] =$countRef;
            foreach($_POST["initType"] as $data){
              $countSource = PHDB::count( $data , array('$and'=>array( $query, array("source.keys" => array('$in' => $tmpSourceKey)))));
            }
            $params["countMenu"]["source"] = $countSource;
            if(count($tmpSourceKey)){
              $query = array('$and' => array( $query , 
                      array("source.keys" => array('$in' => $tmpSourceKey)))
                    );
            }
          }
      }
      if(!empty($mode)){
          $tmpSourceKey = array();
        if($sourceKey != null && $sourceKey != ""){
          //Several Sourcekey
          if(is_array($sourceKey)){
            foreach ($sourceKey as $value) {
              $tmpSourceKey[] = $value;
            }
          }//One Sourcekey
          else{
            $tmpSourceKey[] = $sourceKey;
          }
          $condition=[];
          if($mode=="reference"){
            $origin=(@Yii::app()->session["custom"]) ? "costum" : "network";  
            $condition=array("reference.".$origin => array('$in' => $tmpSourceKey));
          }else if($mode=="source")
              $condition=array("source.keys" => array('$in' => $tmpSourceKey));
          else if($mode=="open")
              $condition=array('$or'=>array(
                  array("preferences.isOpenData"=> true),
                  array("preferences.isOpenData"=> "true")
                  ));
         // if(count($tmpSourceKey)){
            $query = array('$and' => array( $query , 
                    $condition
                    )
                  );
          //}
        }
      }
     // print_r($query);
       //:::::::::::::://////CITOYENS///////////////////////////////////
        $res = array();
        if(@$_POST["initType"]){
          $params["typeDirectory"]=$_POST["initType"];
          foreach($_POST["initType"] as $data){
            $params["results"][$data] = PHDB::findAndLimitAndIndex ( $data , $query, $stepLim, $limitMin);
            $params["results"]["count"][$data] = PHDB::count( $data , $query);
          }
          if($tpl!="json" || $search != ""){
            foreach($_POST["initType"] as $data){
            $params["results"]["count"][$data] = PHDB::count( $data , $query);
            }
          }
        }else{
          $params["typeDirectory"]=[Person::COLLECTION,Project::COLLECTION,Organization::COLLECTION,Event::COLLECTION];
          if(!@$_POST["type"] || $_POST["type"]==Person::COLLECTION){
            $params["results"][Person::COLLECTION] = PHDB::findAndLimitAndIndex ( Person::COLLECTION , $query, $stepLim, $limitMin);
            $params["results"]["count"]["citoyens"] = PHDB::count( Person::COLLECTION , $query);
          }
          else if(@$_POST["type"]){
            //var_dump($_POST["type"]);
            //var_dump($stepLim);
            //var_dump($limitMin);
            //print_r($query);
            $params["results"][$_POST["type"]] = PHDB::findAndLimitAndIndex ( $_POST["type"] , $query, $stepLim, $limitMin);
            //print_r($params["results"][$_POST["type"]]);
            $params["results"]["count"][$_POST["type"]] = PHDB::count( $_POST["type"] , $query);
          }
          if($tpl!="json" || $search != ""){
            $params["results"]["count"]["citoyens"] = PHDB::count( Person::COLLECTION , $query);
            $params["results"]["count"]["organizations"] = PHDB::count( Organization::COLLECTION , $query);
            $params["results"]["count"]["events"] = PHDB::count( Event::COLLECTION , $query);
            $params["results"]["count"]["projects"] = PHDB::count( Project::COLLECTION , $query);
        }
      }

      ///////////////////////////////END CITOYENS //////////////////////////////////////////////

    //  $people = Person::getWhereByLimit(array( "roles"=> array('$exists'=>1)));
      //$counẗPeople=Person::countByWhere(array( "roles"=> array('$exists'=>1)));
      /* **************************************
      *  PROJECTS
      ***************************************** */

     // $params["results"]["organizations"] = array();//$organizations;
      //$params["results"]["projects"] = array();//$projects;
      //$params["results"]["events"] = array();//$events;
      //$params["results"]["countPeople"]=$countAllCitoyen;
      
      //$params["people"] = $people;
      //$params["results"]["superAdmin"] = $superAdmin ;
      //$params["path"] = "../default/";

		  $page = "referenceTable";
      if($tpl=="json")
        Rest::json( $params );
      else{
        if(Yii::app()->request->isAjaxRequest){
          echo $controller->renderPartial($page,$params,true);
        }
        else {
          $controller->render($page,$params);
        }
      }
    }
}
