<?php
class IndexAction extends CAction
{
    public function run($type=null, $id=null, $chart=null){

    	$controller=$this->getController();
    	$this->getController()->layout = "//layouts/empty";
		$params = array( 
			"badges" => PHDB::find(Badge::COLLECTION) 
		);
		
		if( Yii::app()->request->isAjaxRequest )
			echo $controller->renderPartial("index", $params, true);
		else 
			echo $controller->render("index", $params, true);
    }
}